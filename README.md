# Node.js with Express

Serving HTML documents and static content (CSS, JS and Images) using Node and Express. 

## Notes
The app uses `nodemon` to automatically reload the server when changes are made to the source code.

## Run project
Install required dependencies

```bash
npm install
```

Run the project for development
```bash
npm run dev
```

The application will start up on port 5000 by default.

