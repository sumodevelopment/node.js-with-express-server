const express = require('express')
const app = express()
const path = require('path')
const { PORT = 5000 } = process.env

// MIDDLEWARE! WOW! 🤩
app.use(express.static( 
    path.join( __dirname, 'client' )
))
app.use( express.json() )

app.get('/', (request, response) => { 
    return response.sendFile(
        path.join( __dirname, 'client', 'index.html' )
    )
})

app.get('/contact', (request, response) => { 
    return response.sendFile(
        path.join( __dirname, 'client', 'contact.html' )
    )
})

// Sending content (DATA and NOT Html) to my client.
const todos = [
    { id: 1, title: 'Learn express'}, 
    {id:2, title: 'Buy some milk'}, 
    { id:3, title: 'eat some pizza'}
]

app.get('/api/v1/todos', (req, res) => {
    return res.json({ 
        results: todos,
        count: todos.length
    })
})

app.listen( PORT, () => {
    console.log(`Server started on port ${ PORT }`);
})