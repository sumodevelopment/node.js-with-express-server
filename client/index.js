// CLIENT SIDE CODE.
(async function(){
    const elTodoList = document.getElementById('todo-list')
    const elTodoCount = document.getElementById('todo-count')

    const createTodoItem = todo => `<li>${todo.title}</li>`
    const createTodoCount = count => `Todos: ${count}`

    try {
        const response = await fetch('/api/v1/todos').then(r => r.json())
        const { results: todos, count } = response
        elTodoList.innerHTML = todos.map(createTodoItem).join('')
        elTodoCount.innerText = createTodoCount(count)
    } catch (e) {
        console.log(e.message);
    }

})();